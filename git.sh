#!/usr/bin/env perl
# a wrapper to strip --silent off of any git command for the sake of librarian-puppet
use strict;
my ($found_git) = grep { -f $_ && -x $_ && $_ ne $0 } map { $_ . "/git" } split(':', $ENV{PATH});
my $real_git = $ENV{REAL_GIT} || $found_git;
die unless -f $real_git && -x $real_git;
exec($real_git, grep { $_ ne '--silent' } @ARGV);
