#
# In order to use this Gemfile, one must already have Ruby, rbenv, and bundler.
#
# Install Homebrew if you don't already have it:
#    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
#
# Use homebrew to install Ruby and rbenv.
#    brew install ruby rbenv
#
# Then, IN THIS DIRECTORY, install the version of Ruby we want here:
#    rbenv install    # this will get the version we want, referenced in .ruby-version
#
# Next install bundler.  To keep things consistent, use version 1.15.1:
#    gem install --version 1.15.1 bundler
#
# Then use bundler to install all the gems specified in this file:
#    bundle install    # default destination is .bundle
#
#
# Then, for running rspec tests with Vagrant and Virtualbox, something like this:
#
#  bundle exec rspec --no-color spec/acceptance/ 2>&1 | tee ~/acceptance-tests.log
#

source 'https://rubygems.org'

# We demand ruby 1.9.3
#ruby '1.9.3'

# Puppet module editing
gem 'puppet', '4.10.4'
gem 'hiera'
gem 'facter'
gem 'puppet-lint'

# Augeas support
#gem 'ruby-augeas'

# Librarian builds ./modules from ./site
gem 'librarian-puppet'

# Eyaml allows encrypted YAML fields
gem 'hiera-eyaml'
gem 'hiera-eyaml-gpg'

# rake, rspec, and testing gear
#gem 'rake', '>= 10.3.2'
# rspec > 3 leads to deprecation warnings:
# But serverspec requires rspec > 3 so we're living with them.
# https://tickets.puppetlabs.com/browse/PUP-3594
#gem 'rspec'
# rspec-puppet and serverspec appear to be evolving at different rates...
#gem 'rspec-hiera-puppet'
#gem 'rspec-puppet'
#gem 'puppetlabs_spec_helper', '>= 0.4.0'
#gem 'serverspec', '>= 2.3.1'
gem 'colorize'
gem 'net-ssh', '>= 2.9.1', '<3'
#gem 'net-ssh-krb'
#gem 'rspec_junit_formatter'
#gem 'httparty', '>=0.14', '<0.15'

#gem 'fakefs'
#gem 'test-unit'

# Beaker lets us run rspec and serverspec tests
# on test VMs launched by Vagrant.

# We forked beaker to provide our own Vagrantfile
#gem 'beaker',
#  :git => 'http://github.com/mcallaway/beaker.git',
#  :ref => 'mcallawa_vagrantfiles'

#gem 'beaker-librarian'
#gem 'beaker-rspec'

#gem 'gssapi',
#  :git => 'https://github.com/genome-vendor/gssapi',
#  :branch => 'fix_segfault_by_leaking_memory_like_a_noob'

