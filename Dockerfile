
FROM ubuntu:bionic

MAINTAINER Rob Fugina <robf@fugina.com>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y apt-utils && apt-get -y dist-upgrade \
  && apt-get install -y --force-yes --no-install-recommends \
    ruby git \
  && apt-get -y autoremove \
  && apt-get clean

ENV HOME=/root
WORKDIR /root

COPY git.sh /usr/local/bin/git
RUN git clone https://github.com/rbenv/rbenv.git $HOME/.rbenv
ENV PATH=$HOME/.rbenv/bin:$PATH
RUN echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> $HOME/.profile

COPY Gemfile* /root/

#gem install --version 1.15.1 bundler
RUN gem install bundler
RUN bundle install

# tests

RUN git --version && puppet --version && erb --version && puppet-lint --version && bundler --version

# entry point

COPY entrypoint.sh /
ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ ]

